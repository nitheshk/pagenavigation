#Overview

This source code demonstrates the usage of Paginating using Wrapper Class in sales-force.

###Pros and Cons of Choosing This Pagination Tool
**Pros**

* Manages data sets on the server, which reduces page state and increases performance
+ Allows you to paginate over large data sets that have up to 10,000 records.
+ Includes built-in functionality such as next, previous, first, last, getResultSize, and other methods that can simplify your page.
+ Allows you to paginate forward and backward or to any page within the result set.
+ Uses a server-side cursor to cache the entire result set, so results will not change if data changes in the database while a user is paginating from one page to the next.

**Cons**

* Has built-in functionality that results in a very small increase in view state size.
+ Can be used only in Apex.

Before Start with actually code part, i need something to mention about library i have used develop this visual-force page,

* Bootstrap.css

I made you easy and quick solution instead of downloading the all the library again. You can find the **resource.zip** in the  **source Repository**. Insert that into sales-force **Static Resource**. I have used **pageResource** as an resource name, while create a new **Static Resource**.  Resource.zip also contain some images required for this Page.

If you Don't know how to insert resource file into static resource , Please follow this link [Inserting Static Resource](https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_resources_create.htm).

###Code example :

Now Start with Creating the **Pagination** Controller 

```
#!Apex Controller
public with sharing class Pagination {

}
```
i am going to show all the contacts in the form of pagination and there should be one checkbox for each contact to selected or deselect contact to perform delete operation on contacts.

So i need to be create a wrapper class to hold contact and Boolean Variable for selection.

**First create Wrapper class for controller Pagination **

Insert below code into pagination controller.


```
#!java

 public class contactWrapper{
        public Contact cont {get;set;}
        public Boolean isSelected{get;set;}
        public contactWrapper(contact c,Boolean s)
        {
            cont=c;
            isSelected=s;
        }
    }
```

Now Retrieving Data in Apex to Paginate with the help of StandardSetController. The StandardSetController is an extremely powerful tool with built-in functionality that you can use to greatly simplify the custom code in your Visualforce pages. Because the server returns only the data for the page being requested, the StandardSetController can significantly reduce view state, especially compared to the view state you would get while using SOQL.

```
#!java

    Public Integer noOfRecords{get; set;} // Future reference in Visual force Page
    Public Integer size{get;set;}  // Future reference in Visual force Page
    public final Integer Page_Size=10; // Number records in a Page should be displayed 
   
 public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size=Page_Size;
                string queryString = 'Select Id,Name, Email, Birthdate, Phone, MobilePhone from Contact order by Name';
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
```


Now you have the contacts in the Variable **setCon** , whenever your requested for **setCon.getRecords()** it will retrieve the first 10 contact records from the **setCon**.

Here i have create simple wrapper class to show you the demo. You can create your own wrapper class based on requirement. But always you must be aware that **ApexPages.StandardSetController hold only List of sObject, it will not hold the list of wrapper class object **. That's the reason i have written extra code below to accomplish this future in different way.

Below code convert list of contacts into list of wrapper class objects, So that when ever you call **contacts** in visual force page it will receive the list of wrapper class object.

```
#!java


    public list<contactWrapper> contWpr{get;set;} 
    public set<id> selectedContactIds{ get;private set;} // to maintain state of the selected contact
                                                         // through out paginating 

    public Pagination() {
       selectedContactIds=new  set<id>();
    }
     
      Public list<contactWrapper> getContacts(){
         contWpr =new list<contactWrapper>();
        for(Contact c: (List<Contact>)setCon.getRecords())
            if(selectedContactIds.contains(c.id))
                contWpr.add(new contactWrapper(c,true));
                else
                contWpr.add(new contactWrapper(c,false));
        return contWpr;
    }
     

```

###Now you have written code for generating result But how to navigate across the page?

This can be done with easy step with **ApexPages.StandardSetController**.Look at the below code beauty of the StandardSetController, No need to maintain page number,offset and limit etc.. Just use the StandardSetController methods. Copy the below code into **Pagination** controller.


```
#!java

 public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
  
    public void first() {
        setCon.first();
        // do you operation here 
    }
  
    public void last() {
        setCon.last();
            // do you operation here 
    }
  
    public void previous() {
        setCon.previous();
            // do you operation here 
    }
  
    public void next() {
        setCon.next();
            // do you operation here 
    }

```

Your almost done with Paginating the contacts.
Last few methods i have added to fulfill my entire page functionality. As i mention earlier we have additional checkbox for selecting contact and perform delete operation on selected contacts.


```
#!Java

 public void contactSelection()
    {
        Id id=(Id)ApexPages.currentPage().getParameters().get('cId');
        if(selectedContactIds.contains(id))
            selectedContactIds.remove(id);
            else
            selectedContactIds.add(id);
    }
    
 public void deleteContacts()
    {
        List<contact> contactToDelete=[select id from contact where id in :selectedContactIds];
        if(contactToDelete.size()!=0)  //   if(!contactToDelete.isEmpty()) // Best Practice 
           { 
                try {  delete contactToDelete; }  // You may get Exception if you try to delete the 
                                                // related contact ,include try block to avoid error.
                catch(exception ex){ System.debug(ex); }
                refresh();
           }
    }   

 public pageReference refresh() {
        setCon = null;
        selectedContactIds=new set<id>();
        getContacts();
        setCon.setPageNumber(1);
        return null;
    }
     
```

## Coming To visual Force Page

Visual Page look like this before adding any functionality to it.

```
#!visualforce.page

<apex:page controller="Pagination" showHeader="false" title="Contacts" docType="html-5.0"
    standardStyleSheets="false">
    <apex:stylesheet value="{!URLFOR($Resource.pageResource, 'bootstrap.css')}" />

  <style>
       /* all the CSS Class */
  </style>
    <body>
        <div class="main">
            <div class="container-fluid">
                <apex:form>
                  <!-- Render Part comes here -->
                </apex:form>
            </div>
        </div>
    </body>

</apex:page>


```

Copy the below CSS classes between the <style>  </style> tag.

```
#!css

 body{padding: 0% 25% 0% 25%;
        }
        .row {
            align-content: center;
            margin-bottom: 20px;
            min-height: 500px;
            max-height: 500px;
            overflow-y: auto;
        }
        
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            width: 100%;
            height: 250px;
            align-content: center;
            padding: 15px;
            overflow-y: auto scroll;
            overflow-y: hidden;
        }
        
        .card:hover { 
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }
        
        .imgBorder {
            max-width: 60%;
            max-height: 100px;
            min-width: 60%;
            min-height: 100px;
            border-radius: 10px;
        }
        
        .cleaFloat {
            clear: none;
            height: 20px;
        }
        
        #personName,
        #personEmail,
        #personBirthdate {
            border: none;
            text-decoration: none;
            display: block;
            margin: 4px 2px;
            cursor: pointer;
            font-family: monospace;
            font-weight: bold;
        }
        
        #personName {
            color: dodgerblue;
            font-size: 18px;
        }
        
        #personEmail {
            font-size: 12px;
        }
        
        #personBirthdate {
            font-size: 12px;
            font-style: italic;
        }
        
        .likeImg,
        .commentImg {
            width: 36px;
            height: 24px;
            background-repeat: no-repeat;
            float: left;
        }
        
        .likeImg {
             /* background-image: url(https://cdn1.iconfinder.com/data/icons/social-messaging-ui-color-round-2/254000/76-24.png);*/
             background-image:url({!URLFOR($Resource.pageResource, '/like.png')});
        }
        
        .commentImg {
            /* background-image: url(https://cdn1.iconfinder.com/data/icons/social-messaging-ui-color-round-2/254000/87-24.png); */
             background-image:url({!URLFOR($Resource.pageResource, '/comment.png')});
        }

```


Copy the Rendering part of the code between <apex:form> </apex:form>
```
#!html

 <apex:actionFunction name="contactSelection" action="{!contactSelection}" status="fetchStatus"
        reRender="idSection" oncomplete="">
        <apex:param name="cId" value="" />
    </apex:actionFunction>

    <div class="cleaFloat"></div>

    <apex:outputPanel id="section">

        <apex:outputPanel id="idSection">
            <div class="alert alert-info">
                <apex:repeat value="{!selectedContactIds}" var="id">
                    <span>{!id} </span>
                </apex:repeat>
            </div>
        </apex:outputPanel>

    <div class="cleaFloat"></div>

    <div id="NavigationButton1" class="btn-group">

        <apex:commandButton styleClass="btn btn-info" status="fetchStatus" reRender="section"
            value="First" action="{!first}" disabled="{!!hasPrevious}"
            title="First Page" />
        <apex:commandButton styleClass="btn btn-info" status="fetchStatus" reRender="section"
            value="Previous" action="{!previous}" disabled="{!!hasPrevious}"
            title="Previous Page" />

        <button type="button" class="btn btn-default" title="Page Details" >
           {!(pageNumber * size)+1-size}-{!IF((pageNumber * size)>noOfRecords,
            noOfRecords,(pageNumber * size))} of {!noOfRecords}</button>

        <apex:commandButton styleClass="btn btn-info" status="fetchStatus" reRender="section"
            value="Next" action="{!next}" disabled="{!!hasNext}" title="Next Page"/>
        <apex:commandButton styleClass="btn btn-info" status="fetchStatus" reRender="section"
            value="Last" action="{!last}" disabled="{!!hasNext}" title="Last Page" />

    </div>

    <div id="NavigationButton2" class="btn-group">
        <apex:commandButton styleClass="btn btn-success" status="fetchStatus" reRender="section"
            value="Refresh" action="{!refresh}" title="Refresh Page"/>

        <apex:commandButton styleClass="btn btn-danger" status="fetchStatus" reRender="section"
            value="delete" action="{!deleteContacts}" title="delete Contacts" />
    </div>

    <div class="btn-group">
        <apex:actionStatus id="fetchStatus">
            <apex:facet name="start">
                <img src="/img/loading.gif" />
                <span style="color:#4AA02C;font-weight:bold">Loading...</span>
            </apex:facet>
        </apex:actionStatus>
    </div>

    <div class="cleaFloat"></div>


    <div class="row">
        <apex:repeat value="{!contacts}" var="c">
            <div class="col-sm-6 col-md-6 col-xs-6 col-lg-6 ">
                <div class="card">
                 <img class="imgBorder" src="{!URLFOR($Resource.pageResource, '/avatar.png')}" 
                  alt="Avatar"/>
                 <!--    <img class="imgBorder" src="https://www.w3schools.com/howto/img_avatar.png" 
                   alt="Avatar"/> -->
                    <span id="personName"><b>{!c.cont.name}</b></span>
                    <span id="personEmail">{!c.cont.email}</span>
                    <span id="personBirthdate">{!c.cont.Birthdate}</span><br/>

                    <div class="likeImg"> </div>
                    <div class="commentImg"> </div>
                    
                    <apex:inputCheckbox value="{!c.isSelected}" onclick="contactSelection('{!c.cont.id}')"/>

                </div>
            </div>
        </apex:repeat>
    </div>

</apex:outputPanel>

```

Above code is simple and straight forward. 

*  I have added **NavigationButton1** and **NavigationButton2** for navigation of the page with help of   <apex:commandButton/>. When you press navigating button, it will call corresponding apex method and render the **<apex:outputPanel id="section">** 

+  If look at the code inside the <div class="row"> </div> it will display the list wrapper class object By calling getContacts method. 

+  if observer this line of code <apex:inputCheckbox value="{!c.isSelected}" onclick= "contactSelection('{!c.cont.id}')"/>, when you click on check box it will call the action function named as **contactSelection**, and it will render only ** <apex:outputPanel id="idSection"> <apex:outPanel/> ** . This block of code will display the ids of selected contact, i used only for reference part so understanding, can be ignore if don't want.

##Screen Shot
![Screen Shot](/pagination/screenshot.png)